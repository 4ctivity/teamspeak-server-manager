#!/bin/sh

source $ROOT/bin/graphic/CheckerMenu.sh
source $ROOT/bin/graphic/SaveMenu.sh
source $ROOT/bin/graphic/UpdateMenu.sh
source $ROOT/bin/graphic/RestartMenu.sh

BoxTitle="Teamspeak Server Manager"

MenuMain() {

    OPTION=$(whiptail --title "Teamspeak Server Manager" --menu "Choisissez votre distriubtion linux" 15 60 4 \
    "1" "Vérifier le fonctionnement" \
    "2" "Sauvegarder le serveur" \
    "3" "Mettre à jour le serveur" \
    "4" "Redémarrer le serveur"  3>&1 1>&2 2>&3)

    IsUserLeaving

    if [ $OPTION = 1 ]; then
        MenuChecker
    elif [  $OPTION = 2 ]; then
        MenuSave
    elif [  $OPTION = 3 ]; then
        MenuUpdate
    elif [  $OPTION = 4 ]; then
        MenuRestart
    fi

}

Message () {
    whiptail --title "$2" --msgbox "$1" 10 60
}

Error () {
    Message "$1" "Erreur"
    exit
}

IsUserLeaving () {
    exitstatus=$?
    if [ $exitstatus != 0 ]; then
        Message "Vous avez quitté TSM. A bientôt !" "$BoxTitle"
        exit
    fi
}

FieldRequired () {
    if [ -z $1 ]; then
        Message "$2" "Erreur"

        if [ -z $3 ]; then
            MenuMain
        else
            "$3"
        fi
    fi
}

FileExist () {
    if [ ! -f "$1" ]; then
        Message "$2" "Erreur"
        exit
    fi
}
