#!/bin/sh

MenuUpdate() {
    if (whiptail --title "$BoxTitle" --yesno "Voulez vous mettre à jour Teamspeak serveur ?" 10 60) then
        AskPassword
        AskUrl
    else
        MenuMain
    fi
}

AskPassword () {
    PASSWORD=$(whiptail --title "Mot de passe BDD" --passwordbox "Entrer votre mot de passe de la base de données:" 10 60 3>&1 1>&2 2>&3)
    IsUserLeaving
    FieldRequired "$PASSWORD" "Le mot de passedes la BDD est requis" "AskPassword"
}

AskUrl () {
    TS3URL=$(whiptail --title "Input" --inputbox "Saisissez l'URL de la nouvelle version du serveur Teamspeak 3:" 10 60 $lastTeamspeak 3>&1 1>&2 2>&3)
    IsUserLeaving
    FieldRequired "$TS3URL" "Vous devez saisir une URL\nhttps://teamspeak.com/en/downloads/#server\nSERVER 64-BIT X.X.X" "AskUrl"
}