#!/bin/sh

time=$(date "+%d-%m-%Y.%H-%M")

# Effectuer les backups
mkdir $path/backups/$time
cp -r $path/app/current/ $path/backups/$time/serverfiles
FileExist "$path/backups/$time/serverfiles/serverkey.dat" "La copie semble avoir échoué."

mysqldump -u root  --password=$PASSWORD teamspeak > $path/backups/$time/teamspeak.sql || Error "Échec de la sauvegarde de la BDD." && exit
FileExist "$path/backups/$time/teamspeak.sql" "teamspeak.sql n'existe pas."
echo "Le backup se situe dans $path/backups/$time/"

# Téléchargement des nouveaux fichiers
mkdir $path/app/base
cd $path/app/base
wget "$TS3URL"

# Extraire et supprimer l'arichive
arichivename=$(find . -name "*tar*")
tar -xvf $arichivename
rm -rf $arichivename

# Déplacer les fichiers à la racine de "base"
basename=$(find $path/app/base/ -name "*teamspeak*")
mv $basename/* $path/app/base/

cd $path/app/

# Arrêter
echo "Stop all running"
/etc/init.d/cron stop
bash $path/app/current/ts3server_startscript.sh stop
bash $path/app/bots/jts3servermod/jts3servermod_startscript.sh stop

# Mettre à jour le current
cp -r $path/app/base/* $path/app/current/
cp $path/backups/$time/serverfiles/ts3server_startscript.sh $path/app/current/

# Redémarrer
bash $path/app/current/ts3server_startscript.sh start
bash $path/app/bots/jts3servermod/jts3servermod_startscript.sh start
/etc/init.d/cron start

rm -rf $path/app/base

Message "Teamspeak 3 est à jour." "Information"
