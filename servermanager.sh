#!/bin/sh

lastTeamspeak=$(curl https://teamspeak.com/en/downloads/ 2>&1 | grep -o -E 'href="([^"#]+)"' | cut -d'"' -f2 | grep teamspeak3-server_linux_amd64)

# ======================================
# PATHS
# ======================================
ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TS3_CURRENT_PATH="$ROOT/app/current"
BOTS_PATH="$ROOT/app/bots"

# ======================================
# SCRIPTS
# ======================================
# You can edit below.
JTS3SERVERMOD="$BOTS_PATH/jts3servermod/jts3servermod_startscript.sh"

# ======================================
# COLORS
# ======================================
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

# ======================================
# SOURCES
# ======================================
source $ROOT/bin/graphic/MainMenu.sh

# ======================================
# CODE AFTER
# ======================================
do_start() {
	bash $TS3_CURRENT_PATH/ts3server_startscript.sh start
	sleep 3
    bash $JTS3SERVERMOD start
    /etc/init.d/cron start
}

do_stop() {
    /etc/init.d/cron stop
    bash $JTS3SERVERMOD stop
	bash $TS3_CURRENT_PATH/ts3server_startscript.sh stop
}

do_restart() {
	do_stop && do_start
}

do_status() {
	bash $TS3_CURRENT_PATH/ts3server_startscript.sh status
}

do_update() {
	bash $ROOT/bin/update.sh
}

do_checker() {
    tsState=$(do_status)
    if [ "$tsState" != "Server is running" ]; then
		echo -e "Teamspeak server:            [${RED}Die${NC}]"
        do_restart
		do_checker
    else
		echo -e "Teamspeak server:            [${GREEN}Running${NC}]"
	fi

    botsState=$(bash $JTS3SERVERMOD status)
    if [ "$botsState" != "JTS3ServerMod est démarré!" ]; then

		echo -e "Jts3servermod:               [${RED}Die${NC}] "

        bash $JTS3SERVERMOD stop
        bash $JTS3SERVERMOD start
		do_checker
    else
		echo -e "Jts3servermod:               [${GREEN}Running${NC}] "
	fi
}

case "$1" in
start)
	shift
	do_start
	exit $?
	;;
stop)
	do_stop
	exit $?
	;;
restart)
	shift
	do_restart
	exit $?
	;;
status)
	do_status
	exit $?
	;;
update)
	do_update
	exit $?
	;;
checker)
	do_checker
	exit $?
	;;
*)
	# START GRAPHIC MODE IF NOTHING IS IN OPTION
	MenuMain
	exit 2
	;;
esac